﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KontrolaGracza : MonoBehaviour {
    public float speed;
    public Text Wynik;
    public Text Wygrana;
    private int punkcioszki;
    private Rigidbody rb { get; set; }
                                       // Use this for initialization
    void Start () {
        punkcioszki = 0;
        Wynik.text = "Wynik: " + punkcioszki.ToString();
        rb = GetComponent<Rigidbody>();
        Wygrana.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        rb.AddForce(new Vector3(moveHorizontal, 0.0f, moveVertical)*speed);
        if (punkcioszki >= 9)
        {
            Wygrana.text = "Wygrałeś";
            Wygrana.gameObject.SetActive(true);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("t1"))
        {
            other.gameObject.SetActive(false);
            punkcioszki++;
            Wynik.text = "Wynik: " + punkcioszki.ToString();
        }
        else if (other.CompareTag("t2"))
        {
            other.gameObject.SetActive(false);
            punkcioszki+=3;
            Wynik.text = "Wynik: " + punkcioszki.ToString();
        }
    }
}
